cmake_minimum_required(VERSION 2.8.3)
project(slam_msgs)

# Standard dependencies
find_package(catkin REQUIRED COMPONENTS
  std_msgs
  sensor_msgs
  message_generation
)

add_message_files(
  FILES
  keyframeGraphMsg.msg
  framePoseData.msg
  keyframeMsg.msg
  disparityIdepthMsg.msg
  frameMsg.msg
)

generate_messages(DEPENDENCIES std_msgs sensor_msgs)

catkin_package(CATKIN_DEPENDS message_runtime std_msgs sensor_msgs)
